#include <stdio.h>
/*
 * Poniższy program wykonano w języku C. Program jest porosty, jednak sam dostrzegam że dużo lepiej byłoby
 * wykonać go z funkcjami i zdefiniować je w pliku nagłówkowym.
 */

int collection[] = {1,4,5,7,9,10,15,18,20,22,24,25,27,29,30,32,34,35};
int size, low, mid, high, mark;

int main() {

    puts("Zadanie 5: Podaj liczbę którą chces wyszukać: "); //W ramach czytania dokumentacji zamiast printf();

    scanf("%d", &mark); //Zadanie nie precyzowało szukanych liczb więc pobieram je od użytkownika
    printf("Liczba której szukasz w zbiorze to %d", mark);
    low = 0;
    mid = (low+high)/2;
    size = (sizeof(collection) / sizeof(collection[0]));
    high = (size - 1);

    //Procedura wyszukująca liczbę w zbiorze
    while (low <= high) {
        if(collection[mid] < mark)
            low = mid + 1;
        else if (collection[mid] == mark) {
            printf("\nSzukana liczba %d znajduje się w zbiorze na pozycji %d ", mark, mid+1); //C liczy od '0' więc ++
            break;
        }
        else
            high = mid - 1;
        mid = (low + high)/2;
    }
    if(low > high)
        printf("\nSzukana liczba %d NIE znajduje się w zbiorze!", mark);

    puts("\nKoniec programu"); //definitywne wyświetlenie informacji o zakończeniu programu (puts zamiast printf)

    return 0;
}